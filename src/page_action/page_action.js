function fetchSymbol(url) {
	var symbol = '';

	if (url.match('finance.yahoo.com')) {
		symbol = url.replace(/.*finance.yahoo.com\/quote\/([^\/\?]+)([\/\?].*)?/gi, '$1');
	}
	if (url.match('macrotrends.net')) {
		symbol = url.replace(/.*macrotrends.net\/stocks\/charts\/([^\/\?]+)([\/\?].*)?/gi, '$1');
	}
	if (url.match('seekingalpha.com')) {
		symbol = url.replace(/.*seekingalpha.com\/symbol\/([^\/\?]+)([\/\?].*)?/gi, '$1');
	}
	if (url.match('morningstar.com')) {
		symbol = url.replace(/.*morningstar.com\/stocks\/[^\/\?]+\/([^\/\?]+)([\/\?].*)?/gi, '$1');
	}
	if (url.match('finviz.com')) {
		symbol = url.replace(/.*finviz.com\/quote.ashx\?t=([^&]+)(&.*)?/gi, '$1');
	}
	if (url.match('nasdaq.com')) {
		symbol = url.replace(/.*nasdaq.com\/market-activity\/stocks\/([^\/\?]+)([\/\?].*)?/gi, '$1');
	}
	if (url.match('digrin.com')) {
		symbol = url.replace(/.*digrin.com\/stocks\/detail\/([^\/\?]+)([\/\?].*)?/gi, '$1');
	}
	if (url.match('gurufocus.com')) {
		symbol = url.replace(/.*gurufocus.com\/stock\/([^\/\?]+)([\/\?].*)?/gi, '$1');
	}
	if (url.match('tipranks.com')) {
		symbol = url.replace(/.*tipranks.com\/stocks\/([^\/\?]+)([\/\?].*)?/gi, '$1');
	}

	return symbol.toUpperCase();
}

chrome.tabs.query(
	{ active: true, currentWindow: true },
	function callback(tabs) {
		var url = tabs[0].url;
		var symbol = fetchSymbol(url);

		$('a').each(function(){
			var $a = $(this);
			$a.attr(
				'href',
				$a.attr('href').replace(/_XXX_/g, symbol)
			);
		});
	}
);
